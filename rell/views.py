from django.shortcuts import render
from .forms import UserForm,TimelineForm
from .models import User,Timeline
from django.http import HttpResponse
# Create your views here.
def index(request):
    time = Timeline.objects.all()
    return render(request,'rell/index.html',{'time':time})

def ind(request):
    if request.POST:
        # print(request.FILES['image'])

        form=TimelineForm(request.POST,request.FILES)

        if form.is_valid():
            form.save()
            return HttpResponse('success')
        else:
            return HttpResponse('error')
    else:
        form = TimelineForm()
        return render(request,'rell/in.html',{'form':form})


def main(request):
    if request.POST:
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('successfully saved')
    else:
        form = UserForm()
        return render(request,'rell/main.html',{'form':form})


def imageView(request):
    time = Timeline.objects.all()
    arr=[]
    for each_time in time:
        img=each_time.image
        # foto='media'+'/'+ img
        urlPath='/media/'
        print('\n')
        print(img)
        print('\n')
        imgg=each_time.image
        msg=each_time.message_name
        arr.append({
        'url':urlPath+str(img),
        'mg':msg,
        'imgg':imgg
        })
    return render(request,'rell/photos.html',{'time':arr})


def selecteduser(request):
    users = Timeline.objects.all()
    return render(request,'rell/selected.html',{'users':users})


def selected(request,id):
    specificId=Timeline.objects.get(id=id)
    return render(request,'rell/specuser.html',{'specificId':specificId})
