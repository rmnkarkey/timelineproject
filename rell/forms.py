from django import forms
from .models import User,Timeline
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['name']


class TimelineForm(forms.ModelForm):
    class Meta:
        model = Timeline
        fields = ('name','image','message_name')
