from django.urls import path
from . import views

urlpatterns=[
    path('',views.ind,name="index"),
    path('photo/',views.ind,name='two'),
    path('main/',views.main,name='main'),
    path('photos/',views.imageView,name='imageField'),
    path('selecteduser/',views.selecteduser,name='selecteduser'),
    path('selected/<int:id>/',views.selected,name='selected')
]
