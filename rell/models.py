from django.db import models

# Create your models here.
class User(models.Model):
    name=models.CharField(max_length=29)

    def __str__(self):
        return self.name

class Timeline(models.Model):
    name=models.ForeignKey(User,on_delete=models.CASCADE)
    image=models.ImageField(upload_to='media',default=None)
    message_name = models.CharField(max_length=29)
